from random import choice


class Roshambo():

    def __init__(self):
        self.quit = False

    options = ["rock", "paper", "scissors"]

    rules = {("rock", "rock"): 0, ("rock", "paper"): 2,
                                  ("rock", "scissors"): 1,
                                  ("scissors", "scissors"): 0,
                                  ("scissors", "paper"): 1,
                                  ("scissors", "rock"): 2,
                                  ("paper", "paper"): 0,
                                  ("paper", "scissors"): 2,
                                  ("paper", "rock"): 1}

    results = {0: "It's a tie!", 1: "You won!", 2: "You lost!"}

    def computer_choice(self):
        return choice(self.options)

    def take_choice(self):
        while True:
            print("What is your choice? Rock, paper or scissors?")
            player_choice = input().lower()
            if player_choice in self.options:
                return player_choice
            elif player_choice == "q":
                self.quit = True
                break
            else:
                print("Wrong choice! Try again!")
                self.print_line()

    def intro(self):
        print("*********   ROSHAMBO   *********")
        print("Choose Q to quit.")

    def play(self):
        player_choice = self.take_choice()
        if self.quit is True:
            return
        computer_choice = self.computer_choice()
        print("{}\n\n                    VS\n{}".format(
            self.fancy_prints[player_choice],
            self.fancy_prints[computer_choice]))
        game_result = self.rules[(player_choice, computer_choice)]
        print(self.results[int(game_result)])
        self.print_line()

    def print_line(self):
        print("--------------------------------------------------------------")

    fancy_prints = {
    "paper": """
        MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMmdNMMMMMMMMMMMMMMMMM
        MMMMMMMMMMMNNNNNMMMNho/::::::...-odNMMMMMMMMMMMMMM
        MMMMMMMmy+:.`  `:sy.              `-odNMMMMMMMMMMM
        MMMNdo:`          `--`               `-smNMMMMMMMM
        Nmd:-                -:.                 -odNMMMMM
        N/y..:`                .:.`     `......----:+dmNMM
        MNos/`:.                 .:-``-:-....``      .hyyN
        MMMyoo`--          `...----:y+:  -/ossyyyysssohh+h
        MMMMdoy.`:`    `.--.``  ```/mmmooyhyyyyyhhddmmmmmM
        MMMMMNoy/`:-..-.` `-/ssssssyyyyhmNNMMMMMMMMMMMMMMM
        MMMMMMMyos`o``.:osssyyhmNNNNNMMMMMMMMMMMMMMMMMMMMM
        MMMMMMMMd+hy+osyydNNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
        MMMMMMMMMNoyhNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM""",

    "rock": """     
            `-+//++++o++///::::-..`                      
        .+ooso++o+o++///++//++/+++/:-..``              
        yho+oo+++++++++//+/+++ooo++/+/+++/:-.``        
        `shooo++/+o++o+/+++////////::+////+o+++/:-.`    
        /y+oo+o//+//+++/+ooo+oo///:/++/+//+//++++:-`   
        -ho+++o//+++//////++//++//://////://+++++:.`   
        .ys++//////++/+///+/:/++///++/////++++//:.`    
            /o++//+///++//o/://///+//////++/+///:--.`     
            ./osssy//////os+///:////++++oo+/-..--.`      
            `-+oys+//+o++o+:///::::::::----...`        
                `-/+/+++/-:+::/----......-..`           
                    `....--:://::-`````  """,                                                                            

    "scissors": """                                                  
            ``                                    
             --                                   
         ``   -:`                                 
          `..` -/.                                
             .:--/:`                              
               .-://.                             
                 .-:/-                            
                   `:+/-                          
                    `:+o+:`                       
                     `/+/-::hs+-..---`            
                       -+`  .+hNNmo:-/s:          
                       `md`    .om.   .m`         
                        +Nh`     `yy+od/          
                        /NMd-      ` ``           
                        h/`.+s-                   
                        h.   s:                   
                        `oosss`"""}


if __name__ == "__main__":
    game = Roshambo()
    game.intro()
    while game.quit is not True:
        game.play()
