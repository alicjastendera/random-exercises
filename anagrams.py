
def are_anagrams(word1, word2):

    word1 = sorted(word1.lower())
    word2 = sorted(word2.lower())

    x1 = [x for x in word1 if x.isalnum()]
    x2 = [x for x in word2 if x.isalnum()]

    return x1 == x2


if __name__ == "__main__":
    print(are_anagrams("list224e###n", "SI%$^l22eNt"))
