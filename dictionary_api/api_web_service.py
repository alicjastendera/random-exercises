from flask import Flask
from flask_restful import Resource, Api, reqparse

app = Flask(__name__)
api = Api(app)


eng_dict = {"cat": "kot", "bad": "zły, niedobry", "tie": "krawat, remis"}


class EnglishWord(Resource):

    def get(self):
        data = eng_dict  # TODO connect to SQL base
        return data, 200
        # return {'data': data}, 200

    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument("key", required=True)
        parser.add_argument("value", required=True)

        args = parser.parse_args()

        if args['key'] in eng_dict:
            return {"message": f"'{args['key']}' already exists in dictionary."}, 409

        new_data = {args['key']: args['value']}
        eng_dict.update(new_data)
        return eng_dict, 200


class SearchEnglishWord(Resource):
    def get(self, key):
        if key in eng_dict:
            return {key: eng_dict[key]}, 200
        else:
            return {"message": f"'{key}' doesn't exist in dictionary."}, 404

    def put(self, key):
        if key not in eng_dict:
            return {"message": f"'{key}' doesn't exist in dictionary."}, 404
        else:
            parser = reqparse.RequestParser()
            parser.add_argument("value", required=True)
            args = parser.parse_args()
            eng_dict[key] = args["value"]
            return {"message": f"Translation for '{key}' was changed"}, 200

    def delete(self, key):
        if key not in eng_dict:
            return {"message": f"'{key}' doesn't exist in dictionary."}, 404
        else:
            del eng_dict[key]
            return {"message": f"record for '{key}' was deleted"}, 200


class PolishWord(Resource):
    pass


api.add_resource(PolishWord, "/polish")
api.add_resource(SearchEnglishWord, "/english/<key>")
api.add_resource(EnglishWord, "/english")


if __name__ == "__main__":
    app.run()
