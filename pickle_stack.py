import os
from time import sleep
import pickle


class Stack():

    def __init__(self):
        self.unpickle_stack()

    def title_bar(self):
        print("\t***********************")
        print("\t***  STACK CONTENT  ***")
        print("\t {}".format(self.current_stack))
        print("\t***********************")
        print("Options:")
        print("1. PUSH")
        print("2. POP")
        print("3. SIZE")
        print("4. IS EMPTY?")
        print("5. QUIT")

    def take_choice(self):
        print("What do you want to do?")
        action = input()
        return action

    def push(self):
        print("Value to push:")
        value = input()
        self.current_stack.append(value)

    def pop(self):
        if len(self.current_stack) == 0:
            print("Stack already empty!")
            sleep(2)
            return
        self.current_stack.pop()

    def size(self):
        print("Size: {}".format(len(self.current_stack)))
        sleep(2)

    def is_empty(self):
        if len(self.current_stack) == 0:
            print("TRUE")
        else:
            print("FALSE")
        sleep(2)

    def pickle_stack(self):
        pickling_on = open("Stack.pickle", "wb")
        pickle.dump(self.current_stack, pickling_on)
        pickling_on.close()

    def unpickle_stack(self):
        try:
            pickle_off = open("Stack.pickle", "rb")
            self.current_stack = pickle.load(pickle_off)
            print("Last saved stack loaded!")
            pickle_off.close()
        except FileNotFoundError:
            self.current_stack = []


if __name__ == "__main__":
    choice = 0
    stack = Stack()

    while choice != 5:
        stack.title_bar()
        choice = int(stack.take_choice())

        if choice == 1:
            stack.push()
        elif choice == 2:
            stack.pop()
        elif choice == 3:
            stack.size()
        elif choice == 4:
            stack.is_empty()
        else:
            sleep(2)

        os.system('cls')
    stack.pickle_stack()
    print("BYE!")
