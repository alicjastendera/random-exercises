from math import sqrt


class FunkcjaKwadratowa():
    def __init__(self, a, b, c):
        self._a = a
        self._b = b
        self._c = c

    def delta(self):
        return self._b**2 - (4 * self._a * self._c)

    def x_0(self):
        return -self._b / (2 * self._a)

    def x_1(self):
        return (-self._b - sqrt(self.delta())) / (2 * self._a)

    def x_2(self):
        return (-self._b + sqrt(self.delta())) / (2 * self._a)

    def rozwiaz(self):
        if self._a == 0 and self._b == 0:
            if self._c == 0:
                return float("inf")
            else:
                return "Brak rozwiazan"
        if self._a == 0:
            return -self._c / self._b

        delta = self.delta()
        if delta < 0:
            return "Brak rozwiazan"
        if delta == 0:
            return self.x_0()
        else:
            return self.x_1(), self.x_2()
